﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class LoadBuildings : MonoBehaviour
{

	[SerializeField]
	private GameObject building;

	[SerializeField]
	private Transform container;

	private ParseAndGenerate parseAndGenerate;

	private int x = 100;
	private int y = -90;
	private int offset = 180;

	void Start ()
	{
		parseAndGenerate = GetComponent<ParseAndGenerate>();

		string[] folders = System.IO.Directory.GetDirectories(@"Buildings","*", System.IO.SearchOption.AllDirectories);

		for (int i = 0; i < folders.Length; i++)
		{
			//print(folders[i] + " /////////////////////////");

			string[] files = System.IO.Directory.GetFiles(folders[i]);

			GameObject clone = Instantiate(building, container.position, Quaternion.identity);
			clone.transform.SetParent(container);
			clone.SetActive(true);
			clone.GetComponent<RectTransform>().anchoredPosition = new Vector2(x + offset * i, y);
			clone.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);

			for (int j = 0; j < files.Length; j++)
			{
				if (files[j].EndsWith(".jpg") || files[j].EndsWith(".png"))
				{
					clone.GetComponent<Image>().sprite = loadNewSprite(files[j]);	
				}

				if (files[j].EndsWith(".csv"))
				{
					print(files[j]);
					string csvFileName = files[j];
					clone.GetComponent<Button>().onClick.AddListener(delegate { this.setNewPath(csvFileName); } );
				}
			}
		}
	}

	private Sprite loadNewSprite(string filePath, float pixelsPerUnit = 100.0f)
	{

		Sprite newSprite;
		Texture2D spriteTexture = loadTexture(filePath);
		newSprite = Sprite.Create(spriteTexture, new Rect(0, 0, spriteTexture.width, spriteTexture.height), new Vector2(0,0), pixelsPerUnit);

		return newSprite;
	}

	private Texture2D loadTexture(string filePath)
	{
		Texture2D Tex2D;
		byte[] FileData;

		if (File.Exists(filePath))
		{
			FileData = File.ReadAllBytes(filePath);
			Tex2D = new Texture2D(2, 2);

			if (Tex2D.LoadImage(FileData))
			{
				return Tex2D;	
			}
		}
		return null;
	}

	//public void setNewPath()
	public void setNewPath(string path)
	{
		//string path = "Buildings/house/house.csv";
		parseAndGenerate.setPath(path);
	}
}
