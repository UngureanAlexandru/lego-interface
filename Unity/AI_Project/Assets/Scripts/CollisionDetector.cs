﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CollisionDetector : MonoBehaviour {

	private float y;
	public static Text status;

	void Start()
	{
		if (!status)
		{
			status = GameObject.Find("Status").GetComponent<Text>();	
		}
		status.text = "";

		y = transform.position.y;
	}

	void OnCollisionEnter(Collision obj)
	{
		if (obj.gameObject.tag != "Player" && y - transform.position.y > 1)
		{
			//.text = "Unstable construction!";
			print("Make it better");
		}
	}
}
