﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

using  System.Diagnostics;

public struct piece
{
	public GameObject obj;
	public Vector2 type;
	public int orientation;
}

public class ParseAndGenerate : MonoBehaviour {

	private string path = "";

	[SerializeField]
	private GameObject[] legoPieces;

	[SerializeField]
	private GameObject status;

	[SerializeField]
	private GameObject settingsPanel;

	[SerializeField]
	private GameObject menuPanel;

	private string content;
	private string line;

	public Material material;
	private bool finishBuildNow = false;

	private Coroutine coroutine;

	private List<piece> pieces = new List<piece>();
	private bool pauseBuild = false;
	private int fileCursor = 0;

	private Vector2[] types =
	{
		new Vector2(1, 1),
		new Vector2(1, 2),
		new Vector2(1, 3),
		new Vector2(1, 4),
		new Vector2(1, 6),
		new Vector2(1, 8),
		new Vector2(2, 2),
		new Vector2(2, 3),
		new Vector2(2, 4),
		new Vector2(2, 6),
		new Vector2(2, 8)
	};

	[SerializeField]
	private Sprite[] piecesSprite;

	private void Start()
	{
		//coroutine = StartGenerator();
	}

	public void build()
	{
		if (coroutine != null)
		{
			StopCoroutine(coroutine);
		}

		clearScene();
		fileCursor = 0;
		pauseBuild = false;
		coroutine = StartCoroutine(StartGenerator());
	}

	private void clearScene()
	{
		GameObject[] objects = GameObject.FindGameObjectsWithTag("Player");

		foreach (GameObject obj in objects)
		{
			if (obj)
			{
				Destroy(obj);
			}
		}
		pieces.Clear();
	}

	private void enableGravity()
	{
		GameObject[] objects = GameObject.FindGameObjectsWithTag("Player");

		foreach (GameObject obj in objects)
		{
			if (obj.GetComponent<Rigidbody>())
			{
				obj.GetComponent<Rigidbody>().useGravity = true;
				obj.GetComponent<Rigidbody>().isKinematic = false;	
			}
		}
	}

	IEnumerator StartGenerator()
	{
		StreamReader reader = new StreamReader(path);
		reader.BaseStream.Seek(0, SeekOrigin.Begin);

		status.transform.GetChild(0).GetComponent<Text>().text = "";
		status.transform.GetChild(1).GetComponent<Text>().text = "";

		finishBuildNow = false;

		while((line = reader.ReadLine()) != null)
		{
			fileCursor += 1;

			content += line;
			content += '\n';

			string[] data = line.Split(',');

			int id;
			int x, y, z;
			int orientation;

			int.TryParse(data[0], out id);
			int.TryParse(data[1], out x);
			int.TryParse(data[2], out y);
			int.TryParse(data[3], out z);
			int.TryParse(data[5], out orientation);

			Color newColor;
			ColorUtility.TryParseHtmlString(data[4].Trim(), out newColor);

			GameObject newObject = Instantiate(legoPieces[id], new Vector3(x - 0.5f, y, z - 0.5f + 1 * orientation * types[id].x), Quaternion.identity);
			newObject.transform.eulerAngles = new Vector3(0, 90 * orientation, 0);

			newObject.transform.GetChild(0).GetComponent<MeshRenderer>().material = material;
			newObject.transform.GetChild(0).GetComponent<MeshRenderer>().material.color = newColor;
			newObject.tag = "Player";

			newObject.AddComponent<Rigidbody>();
			newObject.GetComponent<Rigidbody>().useGravity = false;
			newObject.GetComponent<Rigidbody>().isKinematic = true;

			piece aux;
			aux.obj = newObject;
			aux.type = types[id];
			aux.orientation = orientation;
			pieces.Add(aux);

			status.transform.GetChild(0).GetComponent<Text>().text = "Piesa: ";
			status.transform.GetChild(1).GetComponent<Text>().text = " se pune pe pozitia: (" + x + ", " + y + " )";  
			status.transform.GetChild(2).GetComponent<Image>().sprite = piecesSprite[id];

			if (pauseBuild == true)
			{
				break;
			}

			if (!finishBuildNow)
			{
				yield return new WaitForSeconds(1);	
			}  
		}

		reader.Close();
		reader = null;

		if (pauseBuild == false)
		{
			mergeObjects();
			enableGravity();
		}

		finishBuildNow = false;
	}

	public void FinishBuild()
	{
		finishBuildNow = true;
	}

	private void AppendText(string row, string file)
	{
		try 
		{
			using (StreamWriter stream = new FileInfo(file).AppendText())
			{
				stream.WriteLine(row);
			}
		}
		catch (FileNotFoundException ex)
		{
			print(ex);
		}
	}

	public void BackButton()
	{
		if (fileCursor == 0)
		{
			return;
		}

		fileCursor--;

		if (pieces[pieces.Count - 1].obj)
		{
			int children = pieces[pieces.Count - 1].obj.transform.childCount - 1; // first element is always the object with the mesh

			for (int i = 0; i < children; i++)
			{
				if (pieces[pieces.Count - 1].obj.transform.GetChild(1).gameObject.tag == "Player")
				{
					pieces[pieces.Count - 1].obj.transform.GetChild(1).transform.parent = null;
				}
			}

			float x = pieces[pieces.Count - 1].obj.transform.position.x;
			float y = pieces[pieces.Count - 1].obj.transform.position.y;
			int id = getIdOfType(pieces[pieces.Count - 1].type);

			status.transform.GetChild(0).GetComponent<Text>().text = "Piesa: ";
			status.transform.GetChild(1).GetComponent<Text>().text = " se pune pe pozitia: (" + x + ", " + y + " )";  
			status.transform.GetChild(2).GetComponent<Image>().sprite = piecesSprite[id];

			GameObject aux = pieces[pieces.Count - 1].obj;
			pieces.RemoveAt(pieces.Count - 1);
			Destroy(aux);
		}
	}

	public void ForwardButton()
	{
		StreamReader reader = new StreamReader(path);

		for (int i = 0; i < fileCursor; i++)
		{
			reader.ReadLine();
		}

		string line;
		if ((line = reader.ReadLine()) == null)
		{
			//mergeObjects();
			enableGravity();
			return;
		}

		fileCursor++;
		print(fileCursor);
		string[] data = line.Split(',');

		int id;
		int x, y, z;
		int orientation;

		int.TryParse(data[0], out id);
		int.TryParse(data[1], out x);
		int.TryParse(data[2], out y);
		int.TryParse(data[3], out z);
		int.TryParse(data[5], out orientation);

		Color newColor;
		ColorUtility.TryParseHtmlString(data[4].Trim(), out newColor);

		GameObject newObject = Instantiate(legoPieces[id], new Vector3(x - 0.5f, y, z - 0.5f + 1 * orientation * types[id].x), Quaternion.identity);
		newObject.transform.eulerAngles = new Vector3(0, 90 * orientation, 0);

		newObject.transform.GetChild(0).GetComponent<MeshRenderer>().material = material;
		newObject.transform.GetChild(0).GetComponent<MeshRenderer>().material.color = newColor;
		newObject.tag = "Player";

		newObject.AddComponent<Rigidbody>();
		newObject.GetComponent<Rigidbody>().useGravity = true;
		newObject.GetComponent<Rigidbody>().isKinematic = false;
		newObject.GetComponent<Rigidbody>().mass = types[id].x * types[id].y;

		status.transform.GetChild(0).GetComponent<Text>().text = "Piesa: ";
		status.transform.GetChild(1).GetComponent<Text>().text = " se pune pe pozitia: (" + x + ", " + y + " )";  
		status.transform.GetChild(2).GetComponent<Image>().sprite = piecesSprite[id];

		piece aux;
		aux.obj = newObject;
		aux.type = types[id];
		aux.orientation = orientation;

		pieces.Add(aux);

		reader.Close();
	}

	public void PauseButton()
	{
		pauseBuild = true;
	}

	private void mergeObjects()
	{
		for (int i = 0; i < pieces.Count; i++)
		{
			for (int j = 0; j < pieces.Count; j++)	
			{
				Vector2 type_i = pieces[i].type;
				Vector2 type_j = pieces[j].type;

				if (pieces[i].orientation == 1)
				{
					type_i = new Vector2(pieces[i].type.y, pieces[i].type.x);
				}

				if (pieces[j].orientation == 1)
				{
					type_j = new Vector2(pieces[j].type.y, pieces[j].type.x);
				}

				float distance = pieces[i].obj.transform.position.y - pieces[j].obj.transform.position.y;
				bool canBeMerged = pieces[i].obj.transform.position.y > pieces[j].obj.transform.position.y && ( (pieces[i].obj.transform.position.x + type_i.x) >= (pieces[j].obj.transform.position.x + type_j.x) && (pieces[i].obj.transform.position.x <= pieces[j].obj.transform.position.x) );

				if (i != j &&  distance < 1.2f && canBeMerged)
				{
					checkBounds(pieces[i].obj, pieces[j].obj);
				}
			}
		}
	}

	private void checkBounds(GameObject obj1, GameObject obj2)
	{
		obj2.transform.parent = obj1.transform;

		if (obj2.GetComponent<Rigidbody>() != null)
		{
			Destroy(obj2.GetComponent<Rigidbody>());
		}
	}

	public void setPath(string newPath)
	{
		path = newPath;
		menuPanel.SetActive(false);
		settingsPanel.SetActive(true);
		clearScene();
	}

	public bool pathIsSet()
	{
		return path.Length > 0;
	}

	public void goToMenu()
	{
		path = "";
		settingsPanel.SetActive(false);
		menuPanel.SetActive(true);
		pauseBuild = true;
		fileCursor = 0;
	}

	public void exit()
	{
		Application.Quit();
	}

	private int getIdOfType(Vector2 type)
	{
		for (int i = 0; i < types.Length; i++)
		{
			if (types[i] == type)
			{
				return i;
			}
		}
		return -1;
	}
}
