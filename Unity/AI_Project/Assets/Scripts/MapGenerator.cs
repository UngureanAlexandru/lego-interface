﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapGenerator : MonoBehaviour {

	[SerializeField]
	private GameObject square1;

	[SerializeField]
	private GameObject square2;

	[SerializeField]
	private GameObject sideSquare;

	[SerializeField]
	int area = 5;

	void Start ()
	{
		for (int i = 0; i < area; i++)
		{
			for (int j = 0; j < area; j++)
			{
				if ((i + j) % 2 == 0)
				{
					Instantiate(square1, new Vector3(i, 0, j), Quaternion.identity);

				}
				else
				{
					Instantiate(square2, new Vector3(i, 0, j), Quaternion.identity);	
				}
			}
		}

		for (int i = 0; i < area; i++)
		{
			GameObject newSideSquare = Instantiate(sideSquare, new Vector3(i, 0, -1), Quaternion.identity);
			newSideSquare.transform.GetChild(0).GetComponent<TextMesh>().text = i + "";
		}

		for (int i = 0; i < area; i++)
		{
			GameObject newSideSquare = Instantiate(sideSquare, new Vector3(-1, 0, i), Quaternion.identity);
			newSideSquare.transform.GetChild(0).GetComponent<TextMesh>().text = i + "";
		}

		// Can't declare newSideSquare variable public. I had to restrain the scope
		{
				// To fill the corner
			GameObject newSideSquare = Instantiate(sideSquare, new Vector3(-1, 0, -1), Quaternion.identity);
			newSideSquare.transform.GetChild(0).GetComponent<TextMesh>().text = "";
		}

	}
}
