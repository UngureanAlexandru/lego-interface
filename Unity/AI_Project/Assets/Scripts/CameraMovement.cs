﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CameraMovement : MonoBehaviour {

	private float movementSpeed = 0.1f;

	private float mouseSensibility = 1;

	private bool lockMovement = true;

	[SerializeField]
	private Slider movementSpeedInput;

	[SerializeField]
	private Slider mouseSensibilityInput;

	[SerializeField]
	private GameObject settingsPanel;

	[SerializeField]
	private GameObject scripts;
	
	void Update () {

		movementSpeed = movementSpeedInput.value;
		mouseSensibility = mouseSensibilityInput.value;
		
		if (!lockMovement)
		{
			if (Input.GetKey(KeyCode.W))
			{
				transform.Translate(Vector3.forward * movementSpeed);
			}

			if (Input.GetKey(KeyCode.A))
			{
				transform.Translate(Vector3.left * movementSpeed);
			}

			if (Input.GetKey(KeyCode.S))
			{
				transform.Translate(Vector3.back * movementSpeed);
			}

			if (Input.GetKey(KeyCode.D))
			{
				transform.Translate(Vector3.right * movementSpeed);
			}

			if (Input.GetKey(KeyCode.E))
			{
				transform.Translate(Vector3.up * movementSpeed);
			}

			if (Input.GetKey(KeyCode.Q))
			{
				transform.Translate(Vector3.down * movementSpeed);
			}

			transform.Rotate(-Input.GetAxis("Mouse Y") * mouseSensibility, Input.GetAxis("Mouse X") * mouseSensibility, 0);

			float x = transform.eulerAngles.x;
			float y = transform.eulerAngles.y;
			float z = 0;
			transform.eulerAngles = new Vector3(x, y, z);
		}

		if (Input.GetKeyUp(KeyCode.Space) && scripts.GetComponent<ParseAndGenerate>().pathIsSet())
		{
			lockMovement = !lockMovement;
		}

		if (Input.GetKeyUp(KeyCode.LeftControl))
		{
			settingsPanel.SetActive(!settingsPanel.activeSelf);
		}	
	}
}
